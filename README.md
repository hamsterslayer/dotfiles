dotfiles
========
Current Setup:
--------------


+ *DISTRO* = Archlinux

+ *KERNEL* = GNU/Linux 3.14.8.2 ck

+ *INIT* = OpenRC

+ *UDEV* = udev

+ *SHELL* = zsh

+ *EDITOR* = vim

+ *MAIL SENDER* = postfix

+ *MAIL RECEIVER* = postfix and offlineimap

+ *MAIL FILTER* = notmuch

+ *MAIL READER* = Mutt

+ *AUDIO PLAYER* = ncmpcpp

+ *VIDEO PLAYER* = mpv

+ *WINDOW MANAGER* = 2bwm and herbstluftwm

+ *TERMINAL EMULATOR* = termite

+ *TERMINAL MULTIPLEX* = tmux

+ *IMAGE VIEWER* = sxiv

+ *WEB BROWSER* = firefox-nightly and dwb


Screenshots:
--------------

Currently:

![ScreenShot1](home/scrot/2bwm-2014-06-24.png)
