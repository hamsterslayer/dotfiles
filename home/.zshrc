#
# ~/.zshrc
#

# tmux/dtach
function dtachd() {
[[ -n "$1" ]] || return 1
dtach -A "${TMPDIR:-/tmp}/$1.dtach" "$@"
}

function tmuxd() {
tmux new-session -s "$1" -A "$@"
}

# source file if it exists
function src() { [[ -f "$1" ]] && source "$1" }

# Source Prezto.
if [[ -s "${ZDOTDIR:-$HOME}/.zprezto/init.zsh" ]]; then
  source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
fi

# Source pkgfile command not found handler
src "/usr/share/doc/pkgfile/command-not-found.zsh"

eval $(dircolors -b $HOME/.dircolors)

#source "/home/axel/.zprezto/zsh-syntax-highlighting-filetypes.zsh"

# Source my aliases
src "$HOME/.zaliases"

PATH="`ruby -e 'puts Gem.user_dir'`/bin:$PATH"

# Source homeshick to manage dotfiles
source "$HOME/.homesick/repos/homeshick/homeshick.sh"
#
# Syntax highlighter styling
#

export PATH="$HOME/.rbenv/bin:$PATH"
eval "$(rbenv init -)"

#
# Syntax highlighter styling
#

# Main
ZSH_HIGHLIGHT_STYLES[default]='none'
ZSH_HIGHLIGHT_STYLES[unknown-token]='fg=red,bold'
ZSH_HIGHLIGHT_STYLES[reserved-word]='fg=yellow'
ZSH_HIGHLIGHT_STYLES[alias]='fg=magenta'
ZSH_HIGHLIGHT_STYLES[builtin]='fg=magenta'
ZSH_HIGHLIGHT_STYLES[function]='fg=magenta'
ZSH_HIGHLIGHT_STYLES[command]='fg=magenta'
ZSH_HIGHLIGHT_STYLES[hashed-command]='fg=blue'
ZSH_HIGHLIGHT_STYLES[precommand]='fg=cyan,underline'
ZSH_HIGHLIGHT_STYLES[commandseparator]='none'
ZSH_HIGHLIGHT_STYLES[path]='underline'
ZSH_HIGHLIGHT_STYLES[path_prefix]='fg=blue,bold,underline'
ZSH_HIGHLIGHT_STYLES[path_approx]='fg=magenta,bold,underline'
ZSH_HIGHLIGHT_STYLES[globbing]='fg=blue'
ZSH_HIGHLIGHT_STYLES[history-expansion]='fg=blue'
ZSH_HIGHLIGHT_STYLES[single-hyphen-option]='none'
ZSH_HIGHLIGHT_STYLES[double-hyphen-option]='none'
ZSH_HIGHLIGHT_STYLES[back-quoted-argument]='fg=blue'
ZSH_HIGHLIGHT_STYLES[single-quoted-argument]='fg=red'
ZSH_HIGHLIGHT_STYLES[double-quoted-argument]='fg=red'
ZSH_HIGHLIGHT_STYLES[dollar-double-quoted-argument]='fg=green'
ZSH_HIGHLIGHT_STYLES[back-double-quoted-argument]='fg=green'
ZSH_HIGHLIGHT_STYLES[assign]='none'

# Brackets
ZSH_HIGHLIGHT_STYLES[bracket-error]='fg=red,bold'
ZSH_HIGHLIGHT_STYLES[bracket-level-1]='fg=green,bold'
ZSH_HIGHLIGHT_STYLES[bracket-level-2]='fg=blue,bold'
ZSH_HIGHLIGHT_STYLES[bracket-level-3]='fg=magenta,bold'
ZSH_HIGHLIGHT_STYLES[bracket-level-4]='fg=red,bold'
ZSH_HIGHLIGHT_STYLES[bracket-level-5]='fg=yellow,bold'
ZSH_HIGHLIGHT_STYLES[bracket-level-6]='fg=cyan,bold'
ZSH_HIGHLIGHT_STYLES[cursor-matchingbracket]='standout'

# Turn `rm -rf <anything>` red
ZSH_HIGHLIGHT_PATTERNS+=('rm*-rf*' 'fg=white,bold,bg=red')

# history-substring-search
HISTORY_SUBSTRING_SEARCH_HIGHLIGHT_FOUND='bold'
HISTORY_SUBSTRING_SEARCH_HIGHLIGHT_NOT_FOUND='bg=default,fg=red,bold'

fpath=($HOME/.homesick/repos/homeshick/completions $fpath)